import os
import joblib
import pandas as pd
from dvclive import Live
from sklearn.metrics import mean_squared_error

EVAL_PATH = "eval"
model_file = 'models/model_1.joblib'
DATA_PATH = 'data/processed'
train_file = os.path.join(DATA_PATH, "train.csv")
test_file = os.path.join(DATA_PATH, "test.csv")
feature_names = ['total_meters']


def evaluate(model, df, split, live):
    """Dump all evaluation metrics and plots for given datasets."""
    X = df[feature_names].to_numpy()
    y = df['price']

    r2 = model.score(X, y)
    # Use dvclive to log a few simple metrics...
    y_pred = model.predict(X)
    rmse = mean_squared_error(y, y_pred, squared=False)
    if not live.summary:
        live.summary = {"R^2": {}, "RMSE": {}}
    live.summary["R^2"][split] = r2
    live.summary["RMSE"][split] = rmse


# Load model and data.
with open(model_file, "rb") as fd:
    model = joblib.load(fd)

train_df = pd.read_csv(train_file)
test_df = pd.read_csv(test_file)


# Evaluate train and test datasets.
live = Live(os.path.join(EVAL_PATH, "live"), dvcyaml=False)
evaluate(model, train_df, "train", live)
evaluate(model, test_df, "test", live)
live.make_summary()
